import os
import json
import shutil
from tqdm import tqdm
import argparse

def move_unused_images(json_path):
    """
    Move unused images from a COCO JSON dataset.

    Args:
        json_path (str): Path to the COCO JSON file.

    Returns:
        None
    """
    # Load COCO data
    with open(json_path) as f:
        coco_data = json.load(f)

    anno_dict = coco_data['annotations']
    image_dict = coco_data['images']

    # Find image IDs with annotations
    anno_img_id_set = {anno['image_id'] for anno in anno_dict}

    # Create a list of image IDs without annotations
    img_ids_to_delete = [img['id'] for img in image_dict if img['id'] not in anno_img_id_set]

    # Remove images without annotations from the image dictionary
    images_to_move = [img for img in image_dict if img['id'] in img_ids_to_delete]

    # Define directory paths
    json_dir_path = os.path.dirname(json_path)
    images_dir = os.path.join(os.path.dirname(json_dir_path), 'images')
    unused_images_dir = os.path.join(os.path.dirname(json_dir_path), 'unused_images')
    if not os.path.exists(unused_images_dir):
        os.mkdir(unused_images_dir)

    # Move unused images to the unused_images_dir
    for img in tqdm(images_to_move, desc="Moving unused images"):
        img_path = os.path.join(images_dir, img['file_name'].split('/')[-1])
        unused_img_path = os.path.join(unused_images_dir, os.path.basename(img['file_name']))
        shutil.move(img_path, unused_img_path)

    # Remove images without annotations from the image dictionary
    image_dict = [img for img in image_dict if img['id'] not in img_ids_to_delete]

    # Remove annotations for images that have been deleted
    anno_dict = [anno for anno in anno_dict if anno['image_id'] not in img_ids_to_delete]

    # Update the image IDs in the annotation dictionary
    for anno in anno_dict:
        old_img_id = anno['image_id']
        new_img_id = old_img_id - img_ids_to_delete.count(old_img_id)
        anno['image_id'] = new_img_id

    # Update the 'images' and 'annotations' keys in the COCO data
    coco_data['images'] = image_dict
    coco_data['annotations'] = anno_dict

    # Save the updated JSON file
    with open(json_path, 'w') as f:
        json.dump(coco_data, f)

    print("Images without annotations have been removed and JSON file has been updated.")

if __name__ == "__main__":
    # Argument parser
    parser = argparse.ArgumentParser(description="Move unused images from COCO JSON dataset.")
    parser.add_argument("--json_path", type=str, required=True, help="Path to the COCO JSON file.")
    args = parser.parse_args()

    # Call the function with provided JSON path
    move_unused_images(args.json_path)
