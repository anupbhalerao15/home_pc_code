.. Remove Unused Images From Dir And Reset JSON documentation master file, created by
   sphinx-quickstart on Fri May 31 00:27:30 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Remove Unused Images From Dir And Reset JSON's documentation!
========================================================================

I. Load COCO Data: Read the JSON file containing COCO dataset information.

II. Extract Image IDs with Annotations: Create a set of image IDs that have annotations.

III. Identify Unused Images: Generate a list of image IDs that do not have any annotations.

IV. Create Directory for Unused Images: Ensure a directory named unused_images exists to store the images without annotations.

V. Move Unused Images: Transfer images without annotations from the images directory to the unused_images directory.

VI. Remove Unused Images from JSON: Exclude images without annotations from the images section in the JSON data.

VII. Remove Annotations for Unused Images: Exclude annotations linked to removed images from the annotations section in the JSON data.

VIII. Update Image IDs in Annotations: Adjust the image IDs in the annotations to account for the removed images.

IX. Save Updated JSON: Write the modified JSON data back to the file, reflecting the removal of images without annotations.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
